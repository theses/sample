[![pipeline status](https://gitlab.com/theses/sample/badges/master/pipeline.svg)](https://gitlab.com/theses/sample/commits/master)

# About

This is a example of a thesis that uses the [style for the Institute of Computing, UNICAMP](https://gitlab.com/theses/ic-tese).

# Setup

## Install Style

To execute this example you need to clone the thesis style locally. The repository should be cloned into your local `texmf` tree.

- In Unix systems:
```bash
mkdir -p ~/texmf/tex/latex/
cd ~/texmf/tex/latex/
git clone git@gitlab.com:theses/ic-tese.git
```

- In macOS system (when latex is installed with mactex):
```bash
mkdir -p ~/Library/texmf/tex/latex/
cd ~/Library/texmf/tex/latex/
git clone git@gitlab.com:theses/ic-tese.git
```

- In other operating systems it may vary depending on your installation. To discover the path you can 
```bash
kpsewhich -var-value TEXMFHOME
```

For more information check the [style repository](https://gitlab.com/theses/ic-tese).

## Write Your Thesis

Modify the `.tex` and `.bib` files within this repository.

Note that it is strongly recommended to maintain this style and the content of your thesis separated.

## Continuous Integration Setup

This example already contains a CI setup in the `.gitab-ci.yml` file. It defaults to the name of the main file, `thesis`. However, you can change the variable `FILE` within the CI setup file to any other file that you find useful.

The CI uses a docker image (`adnrv/texlive:tools`) that contains a TeXLive installation with some "useful" tools. Check [docker hub](https://hub.docker.com/r/adnrv/texlive/) for more information about it.

# Issues 

Please report any issues or bugs using by openning an [issue in this repository](https://gitlab.com/theses/sample/issues). If the problem is regarding the style, report it in its [own issue page](https://gitlab.com/theses/ic-tese/issues).